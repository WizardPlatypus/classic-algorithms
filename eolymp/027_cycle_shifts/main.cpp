// Problem: https://www.e-olymp.com/en/problems/27 
// Solution by: Fyodor Volik 
// Email: f.volik.m@gmail.com

#include <iostream>
#include <cmath>

using namespace std;

uint32_t cycle(uint32_t n, uint32_t t) {
	// get last bit
	auto l= ((1 << t) & n) >> t;
	// clear last bit
	n &= ~(1 << t);
	// shift left
	n <<= 1;
	// set first bit to the state of last
	n |= l;
	return n;
}

int main() {
	uint32_t n, t, m{0};
	cin >> n;
	// get 0-based index of last set bit
	t = (uint32_t)ceil(log2(n + 1)) - 1;
	uint32_t c= n;
	do
	{
		if (c > m) {
			m= c;
		}
		c= cycle(c, t);
	} while (c != n);
	cout << m;
	return 0;
}