#include <iostream>
#include <vector>

using namespace std;

const int VALUES_COUNT = 7;
int values[VALUES_COUNT] = { 1000, 500, 100, 50, 10, 5, 1 };
char romans[VALUES_COUNT] = { 'M', 'D', 'C', 'L', 'X', 'V', 'I' };

int GetValue(char s)
{
    int value = 0;
    switch (s)
    {
    case 'M':
        value = 1000;
        break;
    case 'D':
        value = 500;
        break;
    case 'C':
        value = 100;
        break;
    case 'L':
        value = 50;
        break;
    case 'X':
        value = 10;
        break;
    case 'V':
        value = 5;
        break;
    case 'I':
        value = 1;
        break;
    default:
        value = 0;
        break;
    }
    return value;
}

vector<char> GetRoman(int number)
{
    vector<char> roman;
    int value{0}, c{0};
    int index = 0;
    while (index < VALUES_COUNT and value < number) {
        if (value + values[index] <= number) {
            value += values[index];
            roman.push_back(romans[index]);
            if (++c >= 4) {
                roman.resize(roman.size() - 4);
                if (roman.back() == romans[index - 1]) {
                    roman.resize(roman.size() - 1);
                    roman.push_back(romans[index]);
                    roman.push_back(romans[index - 2]);
//                    roman.emplace(roman.end() - 1, romans[index]);
//                    roman.push_back(romans[index - 2]);
                } else {
                    roman.push_back(romans[index]);
                    roman.push_back(romans[index - 1]);
                }
            }
        } else {
            index++;
            c = 0;
        }
    }
    return roman;
}

int ReadNumber(int begin, int end, string str)
{
    int number{0}, p{GetValue(str[begin])}, s{p};
    for (int i = begin + 1; i < end; ++i) {
        int c{GetValue(str[i])};
        if (c > p) {
            number += c - s;
            s = 0;
        } else if (c < p) {
            number += s;
            s = c;
        } else {
            s += c;
        }
        p = c;
    }
    number += s;
    return number;
}
int main()
{
    // Testing ReadNumber
//    string str;
//    cin >> str;
//    cout << ReadNumber(0, str.size() + 1, str);
    string input;
    cin >> input;
    int plus_index{0};
    for (; plus_index < input.size() && input[plus_index] != '+'; ++plus_index);
    int number1{ReadNumber(0, plus_index, input)}, number2{ReadNumber(plus_index+1, input.size() + 1, input)};
//    cerr << number1 << ' ' << number2 << '\n';
    auto roman = GetRoman(number1 + number2);
    for (auto c : roman) {
        cout << c;
    }
//    cerr << '\n';
//    cerr << number1 + number2 << '\n';
    return 0;
}
