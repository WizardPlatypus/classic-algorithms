#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define ld long double
#define ull unsigned long long
#define uld unsigned long double

#define check(n, pos) (n & (1 << (pos)))
#define biton(n, pos) (n | (1 << (pos)))
#define bitoff(n, pos) (n & ~(1 << (pos)))

#define max(a, b) (a > b ? a : b)
#define min(a, b) (a < b ? a : b)

#define Sort(v) sort(v.begin(), v.end())
#define RSort(v) sort(v.rbegin(), v.rend())
#define CSort(v, C) sort(v.begin(), v.end(), C)

#define all(j) (j).begin(), (j).end()
#define sqr(k) ((k) * (k))

#define PI 3.1415926535897932384626433832795
const ld pi = acos(-1.0);

const int INF = 1e9;
const ll LINF = 1e18;

const ll MOD = 1000000007;
const ll FMOD = 998244353;

const ld eps = 1e-9;

int main() {
  ios_base::sync_with_stdio(NULL);
  cin.tie(NULL);
  cout.tie(NULL);

  ll c;
  cin >> c;
  ll n[c];
  bool check[c];

  for (ll i = 0; i < c; i++) {
    ll cur;
    cin >> cur;
    n[i] = cur;
  }

  for (ll i = 0; i < (c - 1); i++) {
    ll diff = n[i + 1] - n[i];
    if (diff < -1) {
      cout << "No";
      return 0;
    }
    if (diff == -1) {
        if (check[i] == false) {
          check[i+1] = true;
        }
        else if (check[i] == true) {
          cout << "No";
          return 0;
        }
    } else if (diff == 0) {
        check[i + 1] == true;
    }
  }

  cout << "Yes";

  return 0;
}
