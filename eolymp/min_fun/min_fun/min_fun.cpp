using namespace std;

int mod_sum(int a, vector<int> as) {
    int sum{ 0 };
    for (auto ai : as) {
        sum += std::abs(a - ai);
    }
    return sum;
}

int min_index(vector<int> ar) {
    return max_element(ar.begin(), ar.end()) - ar.begin();
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int q;
    cin >> q;

    int B{ 0 };
    vector<int> as{ q }, A{ q };
    for (int t{ 0 }; t < q; ++t) {
        int op;
        cin >> op;
        if (op == 1) {
            int a, b;
            cin >> a >> b;
            int sum{ mod_sum(a, as) };
            A.push_back(sum);
            as.push_back(a);
            B += b;
            continue;
        }
        if (op == 2) {
            int i, x, f_x;
            i = min_index(A);
            x = as[i];
            f_x = B + A[i];
            cout << x << " " << f_x << '\n';
            continue;
        }
    }
}
