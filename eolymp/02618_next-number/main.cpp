#include <iostream>
#include <cstring>
using namespace std;

int main() {
    string number;
    std::cin >> number;
    int l = 1;
    for (auto i = number.rbegin(); i != number.rend(); i++) {
        auto d= (int)*i-48 + l;
        *i=d % 10;
        l = d / 10;
    }
    if (l > 0) cout << l;
    for (const auto& e : number) {
        cout << (int)e;
    }
    cout << '\n';
}
