#include <algorithm>
#include <cstddef>
#include <iostream>
#include <iterator>
#include <vector>

using std::cerr;
using std::cin;
using std::cout;
using std::endl;
using std::size_t;
using std::vector;

int main()
{
  size_t n, m;
  cin >> n >> m;

  vector<size_t> v(n, 0);
  vector<size_t> vi(n, 0);

  for (size_t i = 0; i < n; i++)
  {
    size_t x;
    cin >> x;

    vi[i] = x;
    v[x - 1]++;
  }
  for (auto el : v) cerr << el << " ";
  cerr << '\n';

  for (size_t i = 0; i < m; i++)
  {
    for (auto v : vi) cerr << v << ", ";
    cerr << endl;
    for (auto x : v) cerr << x << ", ";
    cerr << endl;

    size_t p, x;
    cin >> p >> x;

    v[vi[p] - 1]--;
    v[x - 1]++;
    vi[p] = x;

    cout << std::count(v.begin(), v.end(), 0) << endl;
  }
}
