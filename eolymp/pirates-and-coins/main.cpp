#include <bits/stdc++.h>

using namespace std;

int main()
{
    int a, m, n{0};
    cin >> a >> m;
    while (m - a > 0)
    {
        m -= a;
        ++n;
        ++a;
    }
    cout << n;
    return 0;
}
