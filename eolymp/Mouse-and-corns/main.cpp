#include <iostream>
#include <vector>

using namespace std;

vector<int> floor;
vector<long> costs;
vector<bool> dirs;
int m{}, n{};

long Step( int i, int j) {
//    cerr << "Stepping at " << i << ' ' << j << '\n';
    if (i == 0 and j == 0)
        return 0;
    if (costs[i * m + j] != 0) {
        return costs[i * m + j];
    }
    if (i == 0) {
        dirs[j] = true;
        return (costs[j] = Step(0, j - 1) + floor[j]);
    }
    if (j == 0) {
        dirs[i * m] = false;
        return (costs[i * m] = Step(i - 1, 0) + floor[i * m]);
    }

    int r{}, f{};
    r = Step(i, j - 1);
    f = Step(i - 1, j);
    if (r > f) {
        dirs[i * m + j] = true;
        return (costs[i * m + j] = r + floor[i * m + j]);
    } else {
        dirs[i * m + j] = false;
        return (costs[i * m + j] = f + floor[i * m + j]);
    }
}

vector<char>* PathTo(int i, int j) {
//    cerr << "Pathing at " << i << ' ' << j << '\n';
    if (i == 0 and j == 0) {
        vector<char>* path = new vector<char>();
        path->reserve(m + n - 2);
        return path;
    }
    if (dirs[i * m + j]) {
        vector<char>* path = PathTo(i, j - 1);
        path->push_back('R');
        return path;
    } else {
        vector<char>* path = PathTo(i - 1, j);
        path->push_back('F');
        return path;
    }
}

int main()
{
	cin >> m >> n;
	floor.resize( m * n );
	dirs.resize( m * n );
	costs.resize( m * n );
//	cerr << "Resized\n";
	for ( int i{0}; i < m; ++i ) {
		for ( int j{0}; j < n; ++j ) {
			cin >> floor[ i * m + j ];
		}
	}
//	cerr << "Read\n";
	Step(m - 1, n - 1);
//	cerr << "Stepped\n";
//	for (int i{0}; i < m; ++i) {
//        for (int j{0}; j < n; ++j) {
//            cerr << dirs[i * m + j] << ' ';
//        }
//        cerr << '\n';
//	}
	vector<char>* path = PathTo(m - 1, n - 1);
//	cerr << "Pathed\n";
	for (size_t i{0}; i < path->size(); ++i) {
        cout << (*path)[i];
	}
	cout << '\n';
	free(path);
    return 0;
}
