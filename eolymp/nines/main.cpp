#include <bits/stdc++.h>
using namespace std;

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    string str;
    cin >> str;
    for (auto c : str) {
        if (c == '1')
            c = '9';
        else
            c = '1';
        cout << c;
    }
}
