#include <iostream>

using namespace std;

bool IsPrime( int n ) {
	for ( int i{2}; i * i <= n; ++i ) {
		if ( n % i == 0 ) {
			return false;
		}
	}
	return true;
}

int main()
{
	int p{}, b{};
	cin >> p >> b;
	for ( int i{1}; i <= p - 1; ++i ) {
		if ( b + i > 999'999 ) {
			break;
		}
		if ( IsPrime( b + i ) ) {
			cout << i - 1 << '\n';
//			cerr << "The ticket is " << b + i << '\n';
			return 0;
		}
	}
	cout << -1 << '\n';
	return 0;
}
