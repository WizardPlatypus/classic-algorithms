#include <iostream>
#include <limits>
#include <cmath>

using namespace std;

void SolveNeg(int64_t answer, int64_t* a, int n);
void SolveOdd(int64_t answer, int64_t* a, int n);
void SolveEven(int64_t answer, int64_t* a, int n);

int main() {
	int n{};
	cin >> n;

	// BEGIN read array
	int64_t* a = new int64_t[n];
	for (int i= 0; i < n; i++) {
		cin >> a[i];
	}
	// END read array

	// BEGIN calculating sums
	int64_t odd{0}, even{0},
	        neg{numeric_limits<int64_t>::lowest()};
	for (int i= 0; i < n; i++) {
		if (a[i] <= 0) {
			neg = max(neg, a[i]);
		} else if (i % 2 == 0) {
			odd += a[i];
		} else {
			even += a[i];
		}
	}
	// END calculating sums

	/* BEGIN display all sums
	cerr << " odd: " << odd << '\n'
	     << "even: " << even << '\n'
	     << " neg: " << neg << '\n';
	// END display all sums */

	// no positive values
	if (odd == 0 && even == 0) {
		cerr << "Solving neg\n";
		cout << neg << '\n';
		SolveNeg(neg, a, n);
		return 0;
	}

	if (odd >= even) {
		// odd != -1
		cerr << "Solving odd \n";
		cout << odd << '\n';
		SolveOdd(odd, a, n);
	} else {
		// even != -1
		cerr << "Solving even\n";
		cout << even << '\n';
		SolveEven(even, a, n);
	}
	return 0;
}

/*
Input:
5
-77 -77 -77 -3 -43
Output:
-3 : max sum
4 : number of operations
1 : operatoins...
1
1
2
*/
void SolveNeg(int64_t answer, int64_t* a, int n) {
	// number of operations
	cout << n - 1 << '\n';
	// delete evrything before known answer
	int i;
	for (i= 0; i < n; i++) {
		if (a[i] == answer) break;
		cout << 1 << '\n';
	}
	// delete everything after known answer
	for (; n - 1 > i; n--) {
		cout << n - i << '\n';
	}
}

/*
Input:
5
4 -2 -3 3 5
Output:
9 : max sum
2 : number of operations
3 : operations...
2
*/
/*
Input:
6
4 1 7 3 5 6
Output:
16 # max sum
3 : number of operations
6 : operations...
2
2
*/
void SolveOdd(int64_t answer, int64_t* a, int n) {
	// operations
	int* op= new int[n - 1], count= 0;
	// three riders of apocalypsis
	int l{0}, r{0};
	// delete bad
	int d= 0;
	if (a[0] <= 0) {
		d= 1;
		op[count++]= 0;
	}
	for (int i= 2; i < n; i += 2) {
		if (a[i] < 0) {
			op[count++]= i - d;
			d += 2;
		} 
	}
	for (int i= 1; i < n - d; i++) {
		if (i % 2 == 0) {
			op[count++]= 1;
		} else {
			op[count++]= 0;
		}
	}
	// cerr << "deleting right\n";
	for (r= n - 1; // starting from the last element
	     r != 0; // till didn't hit the first one
	     r--) {
		if (r % 2 == 0 && a[r] > 0) break;
		//cerr << r + 1 << '\n';
		op[count++]= r;
	}
	r = n - r;
	// delete left
	cerr << "deleting left\n";
	for (l= 0; l + 1 < n; l += 2) {
		if (a[l] > 0) break;
		//cerr << "1\n1\n";
		op[j++]= 1;
		op[j++]= 1;
	}
	// merge
	cerr << "merging\n";
	while (n - r - l - d > 0) {
		//cerr << "2\n";
		cerr << "j is " << j << ", n is " << n << '\n';
		op[j++]= 2;
		n -= 2;
		if (n == 0) break;
	}
	// print
	cerr << "result:\n";
	cout << j << '\n';
	for (int i= 0; i < j; i++) {
		cout << op[i] << '\n';
	}
}

/*
Input:
5
1 4 3 7 5
Output:
11 # max sum
3 # op count
5
1
2
*/
/*
Input:
4
100 100 -1 100
Output:
200 : max sum 
2 : number of operations
1 : operations...
2
*/
/*
Input:
6
-1 -2 -3 1 2 3
Output:
4
*/
void SolveEven(int64_t answer, int64_t* a, int n) {
	// number of operations
	int j= 0;
	// array of operations
	int* op= new int[n - 1];
	int l{0}, r{0}, d{0};
	// delete right
	for (r= n - 1; r > 0; r--) {
		if (r % 2 == 1 && a[r] > 0) break;
		op[j++]= r + 1;
	}
	r = n - r;
	// delete left
	op[j++]= 1;
	for (l= 1; l + 1 < n; l++) {
		if (l % 2 == 1 && a[l] > 0) break;
		op[j++]= 1;
	}
	// delete bad
	for (int i= l + 2; i < n - r; i += 2) {
		if (a[i] < 0) {
			op[j++]= i - d + 1;
			d += 2;
		}
	}
	// merge
	while (n - r - l - d > 0) {
		op[j++]= 2;
		n -= 2;
	}
	// print
	// cerr << "Result: \n";
	cout << j << '\n';
	for (int i= 0; i < j; i++) {
		cout << op[i] << '\n';
	}
}