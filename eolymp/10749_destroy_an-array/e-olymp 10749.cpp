#include <bits/stdc++.h>

using namespace std;

vector<int> a;
vector<int> op;
int n, t;
bool even_wins;

void delete(int index){
	op.push_back(index);
	if (index == 0)
		even_wins = !even_wins;
	if (index == 0 || index == a.size() - 1) {
		a.erase(a.begin() + index);
		return;
	}
	a[index - 1] += a[index + 1];
	a.erase(a.begin() + index);
	a.erase(a.begin() + index);
}

int find_neg(){
	for (int i= (int)even_wins; i < a.size(); i += 2) {
		if (a[i] <= 0) {
			delete(i);
			return i;
		}
	}
	return -1;
}

void SolveNeg(){
	int max_i= 0;
	for(int i= 1; i < a.size(); i++){
		if (a[i] > a[max_i]) {
			max_i= i;
		}
	}
	cout << a[max_i] << '\n' // max sum
	     << a.size() - 1 << '\n'; // operations number
	for(int i= a.size() - 1; i >= 0; i--) {
		if (i == max_i)
			continue;
		if (i < max_i)
			cout << 1 << '\n';
		else
			cout << i + 1 << '\n';
	}
}

int main()
{
	int64_t even{0}, odd{0};

	// read array
	cin >> n;
	a.reserve(n);
	for(int i= 0; i < n; i++){
		cin >> t;
		a.push_back(t);
		if (t <= 0) continue;
		if (i % 2) // i + 1 is even
			even += t;
		else // i + 1 is odd
			odd += t;
	}
	// if no positive values were given
	if (even == 0 && odd == 0) {
		SolveNeg();
		return 0;
	}
	// find who is a winner
	even_wins = even > odd;
	cout << (even_wins ? even : odd) << '\n'; // max sum

	// delete all negative values from array
	while (find_neg() != -1) {}

	even_wins = !even_wins;
	while (a.size() > 1)
		delete((int)even_wins);

	cout << op.size() << '\n';
	for (int i= 0; i < op.size(); i++) {
		cout << op[i] + 1 << '\n';
	}
}