#include <iostream>
#include <vector>

using namespace std;

int Answer(int k) {
	for (int n{1}; ; n++) {
		int c{0};
		for (int i{1}; i * i <= n; ++i ) {
			if ( n % i == 0 ) {
				c++;
			}
		}
		if ( c == k ) {
			return n;
		}
	}
}

vector<int> GetSimpleDevisors( int n ) {
	vector<int> devisors;
	for ( int d{1}; n != 1; ++d ) {
		if ( n % d == 0 ) {
			n /= d;
			devisors.push_back(d);
			d = 1;
		}
	}
	return devisors;
}

int main()
{
	int K{19};
	for ( int k{1}; k < K; ++k ) {
		int a{Answer(k)};
//		for ( int i{0}; i <= a; i++ ) {
//			cout << '*';
//		}
//		cout << ' ' << k << ": ";
//		cout << a ;
//		for (auto d : GetSimpleDevisors(a) ) {
//			cout << d << ' ';
//		}
		vector<int> devs = GetSimpleDevisors(a);
		for ( int i = 0; i < devs.size(); ++i ) {
			cout << devs[i];
			if (i + 1 != devs.size() ) {
				cout << ' ';
			} else {
				cout << '.';
			}
		}
		cout << ' ' << k << ": " << a;
		cout << '\n';

	}
//	int k{}, n{};
//	cin >> k;
//	for (n = 1; ; n++) {
//		int c{0};
//		for (int i = 1; i * i <= n; ++i) {
//			if (n % i == 0) {
//				c++;
//			}
//		}
////		cerr << n << ": ";
//		for (int i{0}; i < c; ++i) {
//			cerr << '*';
//		}
//		cerr << '\n';
//		if (c == k)
//			break;
//	}
//	cout << n << '\n';
	return 0;
}
