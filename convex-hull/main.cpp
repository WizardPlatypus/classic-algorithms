#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm>
#include <cassert>
#include <cmath>

struct Point { int x{}, y{}; };
using Polygon = std::vector<Point>;

static int L{};
int y( const Point*, const Point* );
bool xComparer( Point a, Point b );
size_t findIndexOfMaxX(Polygon);
size_t findIndexOfMinX(Polygon);

Polygon CH( Polygon );
Polygon merge( Polygon, Polygon );
void UpperTangent( Polygon, Polygon, size_t*, size_t* );
void LowerTangent( Polygon, Polygon, size_t*, size_t* );
int intersection( Point*, Point* );
Point* get( Polygon, size_t, size_t );

bool xComparer( Point a, Point b ) { return a.x < b.x; }
Point* get( Polygon poly, size_t, size_t);
int y( const Point*, const Point*);

Point* get( Polygon poly, size_t base, size_t index ) {
    return &poly[( base + index ) % poly.size()];
}

int y( const Point* from, const Point* to ) {
    double k = (double)(to->y - from->y) / (double)(to->x - from->x);
    double b = from->y - k * from->x;
    return (int)floor((L - b) / k);
}

Polygon CH( Polygon S ) { // For input set S of points:
    // Divide left-half A & right-half B by x coords
    //* i bet it'll work just fine 'cause we've sorted everything at the beginning
    if ( S.size() <= 2 ) {
        return S;
    }
    Polygon A{}; // ...
    A.insert( A.begin(), S.begin(), S.begin() + S.size() / 2 );
    Polygon B{}; // ...
    B.insert( B.begin(), S.begin() + S.size() / 2 + 1, S.end() );
    // Compute CH(A), CH(B)
    A = CH(A);
    B = CH(B);
    // Merge results
    return merge( A, B );
}

Polygon merge( Polygon A, Polygon B ) {
    size_t ai, bi, ak, bk;
    UpperTangent( A, B, &ai, &bi );
    LowerTangent( A, B, &ak, &bk );

    Polygon result = {};//link ai to bi, then bk to bk
    if ( ai < ak ) {
        result.insert( result.begin(), A.begin() + ai, A.begin() + ak + 1 );
    } else if ( ai > ak ) {
        result.insert( result.begin(), A.begin() + ai, A.end() );
        result.insert( result.end() - 1, A.begin(), A.begin() + ak + 1 );
    } else if ( ai == ak ) {
        result.insert( result.begin(), A[ai] );
    }
    if ( bi > bk ) {
        result.insert( result.end() - 1, B.begin() + bk, B.begin() + bi + 1 );
    } else if ( bi < bk ) {
        result.insert( result.end() - 1, B.begin() + bk, B.end() );
        result.insert( result.end() - 1, B.begin(), B.begin() + bi + 1 );
    } else if ( bi == bk ) {
        result.insert( result.begin(), B[bi] );
    }
    return result;
}

size_t findIndexOfMaxX( const Polygon p ) {
    size_t max_i = 0;
    for ( size_t index = 1; index < p.size(); ++index ) {
        if ( p[max_i].x < p[index].x ) {
            max_i = index;
        }
    }
    return max_i;
}

size_t findIndexOfMinX( const Polygon p ) {
    size_t min_i = 0;
    for ( size_t index = 1; index < p.size(); ++index ) {
        if ( p[min_i].x > p[index].x ) {
            min_i = index;
        }
    }
    return min_i;

}

void UpperTangent( Polygon A, Polygon B, size_t* ai, size_t* bi ) {
    auto base_A = findIndexOfMaxX( A );
    auto base_B = findIndexOfMinX( B );
    size_t i = 0, j = 0;
    L = (A[base_A].x + B[base_B].x) / 2 ;

    bool moveRightFinger{}, moveLeftFinger{};
    do {
        moveRightFinger = y( get(A, base_A, i), get(B, base_B, j + 1))
            > y(get(A, base_A, i), get(B, base_B, j));
        moveLeftFinger = y(get(A, base_A, i - 1),get(B, base_B, j))
            > y(get(A, base_A, i), get(B, base_B, j));
        if ( moveRightFinger ) j++;
        if ( moveLeftFinger ) i--;
    } while ( moveRightFinger || moveLeftFinger );

    *ai = (base_A + i) % A.size();
    *bi = (base_B + j) % B.size();
}

void LowerTangent( Polygon A, Polygon B, size_t* ai, size_t* bi ) {
    auto base_A = findIndexOfMaxX( A );
    auto base_B = findIndexOfMinX( B );
    size_t i = 0, j = 0;
    L = (A[base_A].x + B[base_B].x) / 2 ;

    bool moveRightFinger{}, moveLeftFinger{};
    do {
        moveRightFinger = y( get(A, base_A, i), get(B, base_B, j - 1))
            < y(get(A, base_A, i), get(B, base_B, j));
        moveLeftFinger = y(get(A, base_A, i + 1),get(B, base_B, j))
            < y(get(A, base_A, i), get(B, base_B, j));
        if ( moveRightFinger ) j--;
        if ( moveLeftFinger ) i++;
    } while ( moveRightFinger || moveLeftFinger );

    *ai = (base_A + i) % A.size();
    *bi = (base_B + j) % B.size();
}

int main() {
    size_t length;
    std::cin >> length;
    Polygon points{length};
    for ( Point& point: points ) {
        std::cin >> point.x >> point.y;
    }
    std::sort ( points.begin(), points.end(), xComparer );
    Polygon result = CH(points);
    for ( auto point: result ) {
        std::cout << point.x << ' ' << point.y << '\n';
    }
}

