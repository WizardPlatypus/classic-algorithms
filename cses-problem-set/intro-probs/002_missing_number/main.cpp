#include <iostream>

using namespace std;

int main() {
    size_t n;
    cin >> n;
    auto a = new bool[n];
    for (size_t i{0}; i < n - 1; ++i) {
        int32_t j;
        cin >> j;
        a[j - 1] = 1;
    }
    for (size_t i{0}; i < n; ++i) {
        if (!a[i]) {
            cout << i + 1;
            break;
        }
    }
    return 0;
}