#include<iostream>
#include<cmath>
#include<bitset>
using namespace std;
int main(){
	int64_t n, closest=0;
	cin>>n;
	int64_t* a=new int64_t[n];
	int64_t sum=0;
	for(int i=0;i<n;i++){
		cin>>a[i];
		sum+=a[i];
	}
	int64_t half=sum/2;
	for(int64_t s=0;s<(1<<n);s++){
		bitset<20>b(s);
		int64_t c=0;
		for(int64_t i=0;i<n;i++){
			if(b[i]) c+=a[i];
		}
		if(abs(half-c)<abs(half-closest)) closest=c;
	}
	cout<<abs(2*closest-sum);
}