#include<iostream>
using namespace std;
const int LENGTH=8;
bool board[LENGTH*LENGTH];
bool col[LENGTH];
bool diag1[LENGTH*2];
bool diag2[LENGTH*2];
int count=0;
void search(int row_i){
	//cerr<<"Row: "<<row_i<<"; ";
	if(row_i==LENGTH){
		count++;
		//cerr<<"Count: "<<count<<'\n';
		return;
	}
	for(int col_i=0;col_i<LENGTH;col_i++){
		if(board[row_i*LENGTH+col_i] ||
			col[col_i] ||
			diag1[col_i+row_i] ||
			diag2[col_i-row_i+LENGTH-1]) continue;
		// set
		col[col_i]=1;
		diag1[row_i+col_i]=1;
		diag2[col_i-row_i+LENGTH-1]=1;
		// play
		search(row_i+1);
		// reset
		col[col_i]=0;
		diag1[row_i+col_i]=0;
		diag2[col_i-row_i+LENGTH-1]=0;
	}
}
int main(){
	// input
	for(int i=0;i<LENGTH*LENGTH;i++){
		char t;
		cin>>t;
		if(t=='*') board[i]=1;
	}
	search(0);
	cout<<count;
}