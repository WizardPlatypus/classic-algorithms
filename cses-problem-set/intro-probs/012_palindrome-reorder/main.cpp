#include<iostream>
using namespace std;
int main(){
	int* a=new int[26];
	for(int i=0;i<26;i++) a[i]=0;
	char t;
	while(cin>>t){
		a[(int)t-65]++;
		/*
		cerr<<"Got "<<(char)(t)<<", written to "<<((int)t - 65)<<'\n'; // */
	}
	/*
	cerr<<"Status:\n";
	for(int i=0;i<26;i++) cerr<<(char)(i+65)<<": "<<a[i]<<"; ";
	cerr<<'\n'; // */
	char lonely='0';
	for(int i=0;i<26;i++){
		if(a[i]%2){
			if(lonely=='0'){
				lonely=i;
			} else {
				cout<<"NO SOLUTION";
				return 0;
			}
		}
	}
	for(int i=0;i<26;i++){
		int n=a[i]/2;
		while(n--) cout<<(char)(i+65);
	}
	if (lonely!='0'){
		cout<<(char)(lonely+65);
	}
	for(int i=25;i>=0;i--){
		int n=a[i]/2;
		while(n--) cout<<(char)(i+65);
	}
	
}