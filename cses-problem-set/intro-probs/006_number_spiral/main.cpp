#include <iostream>

using namespace std;

/*
int spiral[]= {
    1, 2, 9, 10, 25,
    4, 3, 8, 11, 24,
    5, 6, 7, 12, 23,
    16, 15, 14, 13, 22,
    17, 18, 19, 20, 21
};
*/

int main() {
    int test;
    cin >> test;
    while (test--) {
        uint64_t i, j, n;
        cin >> i >> j;
        if (i <= j) {
            n= j * j - i + 1;
            if (j % 2 == 0) {
                n -= (j - i) * 2;
            }
        } else {
            n= i * i - j + 1;
            if (i % 2 == 1) {
                n -= (i - j) * 2;
            }
        }
        cout << n << '\n';
    }
    return 0;
}