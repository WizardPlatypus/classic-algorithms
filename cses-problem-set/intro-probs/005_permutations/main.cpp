#include <iostream>

using namespace std;

int main() {
    int32_t n{};
    cin >> n;
    if (n == 1) {
        cout << 1;
        return 0;
    }
    if (n < 4) {
        cout << "NO SOLUTION";
        return 0;
    }
    if (n == 4) {
        cout << "3 1 4 2";
        return 0;
    }
    for (int32_t i{1}; i <= n; i += 2) {
        cout << i << ' ';
    }
    for (int32_t i{2}; i <= n; i += 2) {
        cout << i << ' ';
    }
    return 0;
}