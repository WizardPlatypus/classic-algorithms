def main():
	n=int(input())
	sum=n*(n+1)//2
	if sum%2==1:
		print("NO")
		return
	print("YES")
	k=0
	s=0
	t=n
	while True:
		if s+t>=sum//2:
			break
		s+=t
		k+=1
		t-=1
	l=sum//2-s
	print(k+1)
	t=n
	i=0
	while True:
		if i>=k:
			break
		print(t,end=' ')
		t-=1
		i+=1
	print()
	print(l)
	print(n-k-1)
	c=0
	t=1
	while True:
		if c>=n-k-1:
			break
		if t==l:
			continue
		print(t,end=' ')
		c+=1
		t+=1

main()