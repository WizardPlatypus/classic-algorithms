#include <iostream>
#include <cmath>

using namespace std;

int main() {
	uint64_t n;
	cin >> n;
	uint64_t sum = n * (n + 1) / 2;
	if (sum % 2 == 1) {
		cout << "NO";
		return 0;
	} else {
		cout << "YES\n";
	}
	uint64_t k{0}, s{0}, l; // count, sum, last
	for (uint64_t t{n}; s + t < sum / 2; t-- ) {
		s += t;
		k++;
	}
	l= sum / 2 - s;
	cout << k + 1 << '\n';
	for (uint64_t t{n}, i{0}; i < k; i++, t--) {
		cout << t << ' ';
	}
	cout << l << '\n';
	cout << n - k - 1 << '\n';
	for (uint64_t t{1}, c{0}; c < n - k - 1; t++) {
		if (t == l)
			continue;
		cout << t << ' '; 
		c++;
	}
	return 0;
}