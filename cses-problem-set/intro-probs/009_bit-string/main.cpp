#include <iostream>
int main() {
	uint32_t n, r{1};
	std::cin >> n;
	for (uint32_t i{0}; i < n; i++) {
		r <<= 1;
		r %= 1'000'000'000u + 7u;
	}
	std::cout << r;
}