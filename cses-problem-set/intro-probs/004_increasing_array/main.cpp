#include <iostream>

using namespace std;

int main() {
    size_t n{};
    cin >> n;
    auto a = new uint32_t[n];
    for (size_t i{0}; i < n; ++i) {
        cin >> a[i];
    }
    size_t c{0};
    for (size_t i{0}; i + 1 < n; i++) {
        int32_t d = (int32_t)a[i + 1] - (int32_t)a[i];
        if (d < 0) {
            c += (uint32_t)(-d);
            a[i + 1] = a[i];
        }
    }
    cout << c;
    return 0;
}