// Problem: https://cses.fi/problemset/task/1072/
// Solution by: Fyodor Volik
// Email: f.volik.m@gmail.com

#include <iostream>

using namespace std;

uint64_t count(uint64_t k) {
	// combinatorics babe!
	return (k*k*k*k - 9*k*k + 24*k - 16) / 2;
}

int main() {
	uint64_t n;
	cin >> n;
	for (uint64_t k= 1; k <= n; k++) {
		cout << count(k) << '\n';
	}
	return 0;
}