// i found this solution here:
// https://www.geeksforgeeks.org/generate-n-bit-gray-codes/
// couldn't figure out it myself
#include<iostream>
#include<bitset>
using namespace std;
const int MAX_N=16;
int main(){
	std::ios_base::sync_with_stdio(1);
	cin.tie(0);
	cout.tie(0);
	int n;
	cin>>n;
	for(int i=0;i<(1<<n);i++){
		int code=i^(i>>1);
		bitset<MAX_N> b(code);
		for(int j=n-1;j>=0;j--){
			cout<<b[j];
		}
		cout<<'\n';
	}
}