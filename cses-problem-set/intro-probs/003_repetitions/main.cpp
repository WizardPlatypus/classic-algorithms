#include <iostream>

using namespace std;

int main() {
    char l, x;
    uint32_t c{0}, m{0};
    cin.get(l);
    while (cin.get(x)) {
        if (x == l) {
            c++;
        } else {
            l= x;
            if (c > m) {
                m= c;
            }
            c= 0;
        }
    }
    if (c > m)
        m= c;
    cout << m + 1;
}