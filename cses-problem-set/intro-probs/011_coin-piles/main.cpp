#include<iostream>
using namespace std;
int main(){
	int t;
	cin>>t;
	while(t--){
		int a,b;
		cin>>a>>b;
		if(a<b){
			auto t=a;
			a=b;
			b=t;
		}
		if(a>2*b){
			cout<<"NO\n";
			continue;
		}
		if((a+b)%3==0){
			cout<<"YES\n";
		} else {
			cout<<"NO\n";
		}
	}
}