#include<iostream>
#include<vector>
using namespace std;
vector<int> from;
vector<int> to;

void hanoi(int n, int source, int dest, int aux){
	if(n==1){
		//cerr<<source<<' '<<dest<<'\n';
		from.push_back(source);
		to.push_back(dest);
		return;
	}
	hanoi(n-1,source,aux,dest);
	hanoi(1,source,dest,aux);
	hanoi(n-1,aux,dest,source);
	return;
}
int main() {
	int n;
	cin>>n;
	hanoi(n,1,3,2);
	//cerr<<"FINISHED\n";
	cout<<from.size()<<'\n';
	for(size_t i=0;i<from.size();i++){
		cout<<from[i]<<' '<<to[i]<<'\n';
	}
}