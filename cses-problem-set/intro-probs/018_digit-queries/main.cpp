#include<iostream>
#include<string>
//#define DEBUG
using namespace std;
char digit(uint64_t query){
	uint64_t digit_universe=1, factor=1, growth=9*factor*digit_universe, base=0;
	while(base+growth<query){
		base+=growth;
		digit_universe++;
		factor*=10;
		growth=9*factor*digit_universe;
	}
	auto position=query-base-1;
	auto number=factor+position/digit_universe;
	auto digit_index=position%digit_universe;
#ifdef DEBUG
	cerr<<"query: "<<query<<";\n"
	    <<"base: "<<base<<";\n"
	    <<"digit_universe: "<<digit_universe<<";\n"
	    <<"factor: "<<factor<<";\n"
	    <<"position: "<<position<<";\n"
	    <<"number: "<<number<<";\n"
	    <<"digit_index: "<<digit_index<<'\n';
#endif
	return to_string(number)[digit_index];
}
int main(){
	uint64_t q;
	cin>>q;
	while(q--){
		uint64_t n;
		cin>>n;
		cout<<digit(n)<<'\n';
	}
}