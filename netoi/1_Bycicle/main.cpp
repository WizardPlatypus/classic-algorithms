#include<iostream>
#include<string>
#include<cmath>

using namespace std;

int pow_of_ten(int k);
int calc_eights(int n);
int _f[19];
int f(int k) {
	if (k == 0) return 0;
	if (_f[k] != 0) return _f[k];
	_f[k] = 9 * f(k - 1) + pow_of_ten(k - 1);
	return _f[k];
}
int c(int n) {
	// 10^k = q
	int k=0, q=1;
	while(q*10<=n){
		q*=10;
		k++;
	}
	int c=0;
	while(q>1){
		c+= f(k)*(n/q);
		n%=q;
		q/=10;
		k--;
	}
	if(n>=8) c++;
	return c;
}
// a * 10 ^ b
int a_times_10_to_the_power_of_b(int a, int b) {
	a <<= b;
	while (b--)
		a += a << 2;
	return a;
}
int pow_of_ten(int k) {
	return a_times_10_to_the_power_of_b(1, k);
}
int main() {
	int n;
	cin >> n;
	//cerr<<c(n)<<'\n';
	int was, now;
	was = c(n);
	n+=was;
	now = c(n);
	while(was<now){
		n+=now - was;
		was=now;
		now=c(n);
	}
	cout << n;
}