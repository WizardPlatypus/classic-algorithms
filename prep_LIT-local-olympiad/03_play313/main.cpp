#include<iostream>
#include<cstring>
using namespace std;
int factorial(int n){
	int p=1;
	for(int i=2;i<=n;i++) p*=i;
	return p;
}
int selection(int n, int k){
	return factorial(n+k)/factorial(n)/factorial(k);
}
int main(){
	int coins;
	cin>>coins;
	int sum[3];
	int c[3];
	memset(sum,0,3*sizeof(int));
	memset(c,0,3*sizeof(int));
	for(int triples=0;triples*3<=coins;triples++){
		int ones=coins-triples*3;
		int player=(ones+triples)%3;
		c[player]++;
		sum[player]+=selection(triples,ones);
	}
	std::cout<<c[0]<<' '<<c[1]<<' '<<c[2]<<' '<<sum[0]<<' '<<sum[1]<<' '<<sum[2]<<'\n';
	return 0;
}