#include<iostream>
#include<cstring>
using namespace std;
int get_index(int w, int i, int j){
	return (2*w-i+1)*i/2+j;
}
int main(){
	int h,w;
	cin>>h>>w;
	int size=3*h*(w-h+1)/2;
	int* a=new int[size];
	for(int i=0;i<size;i++){
		cin>>a[i];
	}
	int* res=new int[size];
	memset(res,0,size*sizeof(int));
	for(int i=0;i<w;i++) res[i]=a[i];
	for(int i=0;i+1<h;i++){
		for(int j=0;j+i<w;j++){
			int cur=get_index(w,i,j);
			if(i+1<h&&j-1>=0){
				int left=get_index(w,i+1,j-1);
				res[left]=max(res[left],a[left]+res[cur]);
			}
			if(i+1<h&&j+i+1<w){
				int right=get_index(w,i+1,j);
				res[right]=max(res[right],a[right]+res[cur]);
			}
			if(i+2<h&&j-1>=0){
				int down=get_index(w,i+2,j-1);
				res[down]=max(res[down],a[down]+res[cur]);
			}
		}
	}
	int mmax=-1;
	for(int i=size-h+1;i<size;i++){
		if(res[i]>mmax){
			mmax=res[i];
		}
	}
	cout<<mmax;
}