#include <iostream>
#include <vector>
#include <algorithm>

int main() {
  // sum of positives
  // product of numbers between min and max elements
  int N{}, sum_of_positive{0}, product_in_between{1};
  std::cin >> N;
  std::vector<int> numbers(N);
  for ( auto& number: numbers ) {
    std::cin >> number;
    if (number > 0) {
      sum_of_positive += number;
    }
  }

  auto max_iter = std::max_element(numbers.begin(), numbers.end());
  auto min_iter = std::min_element(numbers.begin(), numbers.end());
  auto begin = 1 + std::min(max_iter, min_iter);
  auto end = std::max(max_iter, min_iter);
  //std::cerr << *begin << " " << *end;
  for ( auto i = begin; i != end; ++i ) {
    product_in_between *= *i;
  }
  std::cout << sum_of_positive << " " << product_in_between;
}
