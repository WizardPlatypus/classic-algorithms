#include <iostream>

int main() {
    /* evil solution
    unsigned long long n{};
    std::cin >> n;
    std::cout << n * n;
    // */
    // /* normal solution
    unsigned long long n{};
    std::cin >> n;
    n /= 10;
    n = n * (n + 1) * 100 + 25;
    std::cout << n;
    // */
}
