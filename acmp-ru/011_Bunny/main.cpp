#include <iostream>
#include <deque>
#include <numeric>

using ull = unsigned long long int;

int main() {
  // k - max length of jump
  // n - whole length of staircase
  int k{}, n{};
  std::cin >> k >> n;
  std::deque<ull> last_jumps{};
  for ( int i = 0; i < k; ++i ) {
    auto new_jump = std::accumulate(last_jumps.begin(),
        last_jumps.end(), 1);
    last_jumps.push_back(new_jump);
  }
  ull the_last_one{};
  for ( size_t i = 0; i < n - k; ++i ) {
    the_last_one = std::accumulate(last_jumps.begin(),
        last_jumps.end(), 0);
    last_jumps.push_back(the_last_one);
    last_jumps.pop_front();
  }
  std::cout << last_jumps.back();
}
