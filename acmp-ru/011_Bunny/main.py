# solution from my friend Taras
# sometimes bignum support out of the box matters
# thank you python!
from collections import deque

s = input().split(" ")
k = int(s[0])
n = int(s[1])

last_jumps = deque()

for i in range(0, k):
    new_jump = 1

    for i in last_jumps:
        new_jump += i

    last_jumps.append(new_jump)

for i in range(0, n - k):
    the_last_one = 0

    for i in last_jumps:
        the_last_one += i

    last_jumps.append(the_last_one)
    last_jumps.popleft()

print(last_jumps[len(last_jumps) - 1])
