#include <iostream>

int main() {
  unsigned long a{}, b{}, c{};
  std::cin >> a >> b >> c;
  if ( a * b == c ) {
    std::cout << "YES";
  } else {
    std::cout << "NO";
  }
}
