#include <iostream>
#include <algorithm>
#include <vector>
#include <cmath>

struct Point{
  int x{}, y{};
};

struct Cottager{
  Point target{};
  Point dacha[4]{};
  bool check() const {
    int higher{0}, lower{0};
    for ( int i = 0; i < 4; ++i ) {
      int j = (i + 1) % 4;
      int y{};
      if ( cross(dacha[i], dacha[j], target.x, y) ) {
        if ( y > target.y ) {
          ++higher;
        } else if ( y < target.y ) {
          ++lower;
        }
      }
    }
    return higher % 2 == 1 && lower % 2 == 1;
  }
  bool cross( const Point a, const Point b, const int x, int& y ) const {
    auto min_x = std::min(a.x, b.x);
    auto max_x = std::max(a.x, b.x);
    bool isInside = x >= min_x && x < max_x;
    if ( !isInside ) {
      return false;
    }
    double k = (double)(b.y - a.y) / (double)(b.x - a.x);
    double s = a.y - k * a.x;
    y = (int)floor(x * k + s);
    return true;
  }
};

int main() {
  int n{}; // number of cottagers
  std::cin >> n;
  std::vector<Cottager> neighbors(n);
  for ( auto& neighbor: neighbors ) {
    std::cin >> neighbor.target.x >> neighbor.target.y
      >> neighbor.dacha[0].x >> neighbor.dacha[0].y
      >> neighbor.dacha[1].x >> neighbor.dacha[1].y
      >> neighbor.dacha[2].x >> neighbor.dacha[2].y
      >> neighbor.dacha[3].x >> neighbor.dacha[3].y;
  }
  int count{0};
  for ( const auto neighbor: neighbors ) {
    if ( neighbor.check() ) ++count;
  }
  std::cout << count;
}
