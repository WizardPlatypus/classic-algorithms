#include <iostream>

inline long y( long a, long b, long c, long d, long x) {
  return a * x * x * x + b * x * x + c * x + d;
}

int main() {
  // a * x^3 + b * x^2 + c * x + d;
  long a{}, b{}, c{}, d{};
  std::cin >> a >> b >> c >> d;
  for ( long x = -100; x <= 100; ++x ) {
    if ( y( a, b, c, d, x ) == 0 ) {
      std::cout << x << " ";
    }
  }
}
