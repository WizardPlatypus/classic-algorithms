#include <iostream>

int sum( int n) {
    return n * (n + 1) / 2;
}

int main() {
    int n{};
    std::cin >> n;
    if ( n > 0 ) {
        std::cout << sum(n);
    } else if ( n < 0 ) {
        std::cout << (sum(n * -1) - 1) * -1;
    } else if ( n == 0 ) {
        std::cout << 1;
    } else {
        std::cerr << "Never";
    }
}
