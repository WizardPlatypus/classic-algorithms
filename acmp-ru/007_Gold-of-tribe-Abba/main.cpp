#include <iostream>
#include <string>
using std::string;

string biggest( string num1, string num2 ) {
  if ( num1.length() > num2.length() ) {
    return num1;
  } else if ( num1.length() < num2.length() ) {
    return num2;
  } else { // num1.length() == num2.length()
    for ( size_t i = 0; i < num1.length(); ++i ) {
      if ( num1[i] > num2[i] ) {
//        std::cerr << ":" << num1[i] << ">" << num2[i] << "\n";
        return num1;
      } else if ( num1[i] < num2[i] ) {
//        std::cerr << ":" << num1[i] << "<" << num2[i] << "\n";
        return num2;
      } else { // num1[i] == num2[i]
//        std::cerr << ":" << num1[i] << "=" << num2[i] << "\n";
        continue;
      }
    }
    // in case num1 is actually equal to num2
    return num1;
  }
}


int main() {
  string number1{}, number2{}, number3{};
  std::cin >> number1 >> number2 >> number3;
  string big_one = biggest(number1, number2);
  big_one = biggest(big_one, number3);
  std::cout << big_one;
}
