#include <iostream>
#include <string>

using std::string;

bool letter(int index, string source, short& number) {
  char victim = source[index];
  string letters{"ABCDEFGH"};
  for ( number = 0; number < letters.length(); ++number ) {
    if ( victim == letters[number] ) {
      return true;
    }
  }
  return false;
}
bool dash(int index, string source) {
  return source[index] == '-';
}
bool number(int index, string source, short& number) {
  char victim = source[index];
  string nums{"12345678"};
  for ( number = 0; number < nums.length(); ++number ) {
    if ( victim == nums[number] ) {
      return true;
    }
  }
  return false;
}
bool correctMove( short let_from, short num_from,
    short let_to, short num_to ) {
  short dnum = num_to - num_from;
  if (dnum < 0) dnum *= -1;
  short dlet = let_to - let_from;
  if (dlet < 0) dlet *= -1;
  return (dnum == 1 && dlet == 2)
    || (dnum == 2 && dlet == 1);
}


int main() {
  string move{};
  std::cin >> move;
  short num_from{}, let_from{}, num_to{}, let_to{};
  if ( move.length() < 5
    || !letter(0, move, let_from)
    || !number(1, move, num_from)
    || !dash(2, move)
    || !letter(3, move, let_to)
    || !number(4, move, num_to) ) {
    std::cout << "ERROR";
  } else if ( correctMove( let_from, num_from,
      let_to, num_to ) ) {
    std::cout << "YES";
  } else {
    std::cout << "NO";
  }
}
