#include <iostream>
#include <vector>

int main() {
    unsigned short N{};
    std::cin >> N;
    std::vector<short> even{}, odd{};
    for ( int i = 0; i < N; ++i ) {
        short date{};
        std::cin >> date;
        if ( date % 2 == 0 ) {
            even.push_back(date);
        } else {
            odd.push_back(date);
        }
    }
    for ( auto date: odd ) {
        std::cout << date << " ";
    }
    std::cout << "\n";
    for ( auto date: even ) {
        std::cout << date << " ";
    }
    std::cout << "\n";
    if ( even.size() >= odd.size() ) {
        std::cout << "YES";
    } else {
        std::cout << "NO";
    }
}
