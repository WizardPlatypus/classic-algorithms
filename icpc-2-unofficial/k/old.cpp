#include<iostream>
#include<map>
using namespace std;


int main() {
  map<int, int> m;
  int n;
  cin>>n;
  bool* a = new bool[n];

  int first=-1, last;
  for(int i=0;i<n;i++){
    char cell;
    cin>>cell;
    a[i]=(cell=='O');
    if(a[i]){
      if(first==-1) first=i;
      last=i;
    }
  }
  for(int i=first;i<=last;i++){
    int x=0;
    for(int j=i+1;j<=last;j++){
      if(a[j]) break;
      x++;
    }
    m.insert(i,x);
    i+=x;
  }

  int k;
  cin>>k;
  int sum=0;
  for (auto e : m) {
    sum += e.second;
  }
  while(k--) {
    int index;
    cin>>index;
    index--;
    a[index] = !a[index];
    bool O = a[index];
    first = m.begin()->first;
    last = m.rbegin()->first;
    if (O) {
      if (index < first) {
        auto _this = m.insert(index,0).first;
        int change;
        _this->second=(change=(++_this)->first-index-1);
        sum += change;
        first = index;
      } else if (index > last) {
        auto prev = find_prev(m, m.upper_bound(index)->first);
        m[index] = 0;
        prev->second = index - prev->first - 1;
        sum+=prev->second;
        last = index;
      } else {
        auto prev = find_prev(m, m.upper_bound(index)->first);
        auto next = m.upper_bound(index);
        prev->second = index - prev->first - 1;
        m[index] = next->first - index - 1;
        sum -= 1;
      }
    } else {
      if (index == first) {
        auto next = m.upper_bound(index);
        sum -= m[index];
        first = next->first;
        m.erase(index);
      } else if (index == last) {
        auto prev = find_prev(m, m.upper_bound(index)->first);
        sum -= prev->second;
        prev->second = 0;
        last = prev->first;
        m.erase(index);
      } else {
        auto prev = find_prev(m, m.upper_bound(index)->first);
        prev->second += m[index]+1;
        m.erase(index);
        sum += 1;
      }
    }
    cout << sum + min(first, n - last - 1) << "\n";
  }

  return 0;
}
