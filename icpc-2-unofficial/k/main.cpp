#include<iostream>
#include<set>
using namespace std;
int main(){
	int n, k=0, q;
	set<int> s;
	cin>>n;
	for(int i=0;i<n;i++){
		char cell;
		cin>>cell;
		if(cell=='O'){
			k++;
			s.insert(i+1);
			//cerr<<i+1<<' ';
		}
	}
	//cerr<<'\n';
	cin>>q;
	while(q--){
		int index;
		cin>>index;
		if(s.count(index)){
			s.erase(index);
			k--;
		} else {
			s.insert(index);
			k++;
		}
		//cerr<<"k = "<<k<<"; size = "<<s.size()<<'\n';
		int l=*s.begin(), r=*s.rbegin();
		//cerr<<"l = "<<l<<"; r = "<<r<<'\n';
		cout<<min(r,n-l+1)-k<<'\n';
	}
}