#include<iostream>
#include<limits>
#include<cstring>
using namespace std;
uint64_t* m;
int n;
uint64_t search(int last, int visited){
	uint64_t max_p=0;
	uint64_t sum=0;
	for(int i=1;i<n;i++){
		if(visited&(1<<i)) continue;
		if(m[last*n+i]==0) continue;
		visited|=(1<<i);
		auto p=m[last*n+i]+search(i,visited);
		//cerr<<"From "<<last<<" to "<<i<<": "<<p<<'\n';
		if(p>max_p) {
			sum+=max_p;
			max_p=p;
		} else {
			sum+=p;
		}
		visited^=(1<<i);
	}
	if(max_p==0)
		return 0;
	sum*=2;
	sum+=max_p;
	return sum;
}
int main(){
	cin>>n;
	m=new uint64_t[n*n];
	memset(m,0,sizeof(uint64_t)*n*n);
	int* d=new int[n];
	memset(d,0,sizeof(int)*n);
	for(int i=0;i<n-1;i++){
		int a, b;
		uint64_t l;
		cin>>a>>b>>l;
		a--;
		b--;
		m[a*n+b]=m[b*n+a]=l;
		d[a]++;
		d[b]++;
	}
	int visited=0;
	for(int i=1;i<n;i++){
		if(d[i]==1) visited|=(1<<i);
	}
	visited|=1;
	uint64_t res = search(0, visited);
	cout<<res;
}