numb = input()
numb = numb[::-1]

result = ""
l = len(numb)
for i in range(l):
    result += numb[i]
    if (i + 1) % 3 == 0 and (i + 1) != l and numb[i + 1] != "-":
        result += ","

result = result[::-1]
print(result)