#include <iostream>

int main()
{
  int w, h, x, y, t;
  std::cin >> w >> h >> x >> y >> t;

  x--;
  y--;

  bool *map = new bool[w * h];

  for (int i = 0; i < w * h; i++)
    map[i] = true;

  map[y * h + x] = false;

  for (int i = 0; i < t; i++)
  {
    char d;
    int  dx {0}, dy {0}, s;
    std::cin >> d >> s;

    switch (d) {
    case 'L':
      dx = -1;
      break;
    case 'R':
      dx = 1;
      break;
    case 'U':
      dy = -1;
      break;
    case 'D':
      dy = 1;
      break;
    }

    for (int i = 0; i < s; i++)
    {
      x += dx;
      y += dy;
      map[y * h + x] = false;
    }
  }

  for (int y = 0; y < h; y++)
  {
    for (int x = 0; x < w; x++)
      std::cout << (map[y * h + x] ? '.' : '*');
    std::cout << std::endl;
  }
}