#include <algorithm>
#include <cstddef>
#include <iostream>
#include <iterator>
#include <ostream>
#include <queue>
#include <set>
#include <vector>

using node  = std::size_t;
using graph = std::vector<std::set<node>>;

bool has_cycles(graph g)
{
  for (int i = 0; i < g.size(); i++)
  {
    if (g[i].size() == 0)
    {
      return false;
    }
  }

  return true;
}

int main()
{
  int n, m;
  std::cin >> n >> m;

  graph f(n), g(n);

  for (int i = 0; i < m; i++)
  {
    int a, b;
    std::cin >> a >> b;

    g[b - 1].insert(a - 1);
    f[a - 1].insert(b - 1);
  }

  if (has_cycles(g)) {
    std::cout << "No" << std::endl;
    return 0;
  }
  std::cout << "Yes" << std::endl;
  std::vector<node> qs;
  for (node i = 0; i < n; i++)
    if (f[i].empty())
      qs.push_back(i);

  for (auto i : qs) {
    std::vector<node> s, v;
    s.push_back(i);
    v.reserve(n);

    std::size_t c = 0;

    while (!s.empty())
    {
      node n = s.back();
      s.pop_back();
      v[n] = c++;
      for (auto x : g[n])
      {
        f[x].erase(n);
        if (f[x].empty())
          s.push_back(x);
      }
    }
    std::vector<node> p(c);
    for (int i = 0; i < v.size(); i++)
      p[v[i]] = i;
    for (auto x : v)
      std::cout << x + 1 << " ";
    std::cout << std::endl;
  }
}