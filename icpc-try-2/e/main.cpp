#include<iostream>
#include<map>
#include<set>
using namespace std;
int count(map<string,int> m){
	int c=0;
	for(auto e=m.begin();e!=m.end();e++){
		c+=e->second;
	}
	return c;
}
int main(){
	int n;
	cin>>n;
	map<string,map<string,int>> m;
	while(n--){
		string sender;
		string reciever;
		cin>>sender>>reciever;
		m[reciever][sender]++;
	}
	for(auto e: m){
		cout<<e.first<<' '<<count(e.second)<<'\n';
		int i=0;
		for(auto k=e.second.begin(); k!=e.second.end();k++){
			cout<<k->first;
			if(i+1<(int)e.second.size()){
				cout<<",";
			}
			i++;
		}
		cout<<'\n';
	}
}