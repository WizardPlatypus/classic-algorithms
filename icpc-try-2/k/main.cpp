#include<iostream>
using namespace std;
struct Node{
	int data=0;
	Node* left=nullptr, *right=nullptr;
	~Node(){
		free(left);
		free(right);
	}
};
int insert(Node* parent, int data){
	if (parent->data==data) return 0;
	if (data<parent->data){
		if (parent->left==nullptr){
			parent->left=new Node{data};
			return 1;
		}
		return 1+insert(parent->left, data);
	} else {
		if (parent->right==nullptr){
			parent->right=new Node{data};
			return 1;
		}
		return 1+insert(parent->right,data);
	}
}

int main(){
	int n;
	cin>>n;
	int max_depth=-1, max_j=0;
	for(int j=1;j<=n;j++){
		int m;
		cin>>m;
		Node origin;
		cin>>origin.data;
		int depth=0;
		for(int i=1;i<m;i++){
			int data, d;
			cin>>data;
			d=insert(&origin, data);
			if(d>depth){
				depth=d;
			}
		}
		if(depth>max_depth){
			max_depth=depth;
			max_j=j;
		}
	}
	cout<<max_j<<' '<<max_depth+1;
}