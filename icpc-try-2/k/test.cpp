#include <iostream>

using namespace std;

struct Node {
  int data;
  Node *left, *right;
};

int add(Node *big, Node cur, int height) {
  if (cur.data >= big->data) {
    if (big->right == nullptr) {
      big->right = cur.right;
      height++;
    } else {
      height = 1 + add(big->right, cur, height);
    }
  } else {
    if (big->left == nullptr) {
      big->left = cur.left;
      height++;
    } else {
      height = 1 + add(big->left, cur, height);
    }
  }

  return height;
}

int main() {
  ios_base::sync_with_stdio(NULL);
  cin.tie(NULL);
  cout.tie(NULL);

  int m, height = 0, index;
  cin >> m;

  Node arr[m];
  for (int i = 0; i < m; i++) {
    int temp;
    cin >> temp;

    for (int j = 0; j < temp; j++) {
      int numb;
      cin >> numb;

      Node cur = {numb};

      if (j == 0) {
        arr[i] = cur;
      } else {
        int cur_height = add(&arr[i], cur, 0);
        if (cur_height > height) {
          height = cur_height;
          index = i;
        }
        // cout << cur_height << endl;
      }
    }
  }

  cout << (index + 1) << " " << height;

  return 0;
}