#include <iostream>
#include <ostream>
int main()
{
  int d;
  std::cin >> d;

  for (int i = 0; i * 8 <= d; i++)
  {
    if ((d - i * 8) % 7 == 0)
    {
      std::cout << "YES" << std::endl;
      return 0;
    }
  }

  std::cout << "NO" << std::endl;
}