#include <iostream> // cin, cout, cerr, endl

const int MAX_ORDER{100};
int order;
int adjecency[MAX_ORDER][MAX_ORDER];
bool visited[MAX_ORDER]{};

void DFS_REC( int current, int indent = {0} ) {
  for ( int i{0}; i < indent; ++i )
    std::cout << " ";
  std::cout << current + 1 << std::endl;

  visited[current] = true;
  for ( int victim{0}; victim < order; ++victim ) {
    bool isIncident = adjecency[current][victim] != 0;
    bool isUnvisited = not visited[victim];
    if ( isIncident and isUnvisited ) {
      DFS_REC( victim, indent + 1 );
    }
  }
}

int main() {
  std::cin >> order;
  if ( order > MAX_ORDER ) {
    std::cerr << "Order is too big";
    return 1;
  }

  for ( int i{0}; i < order; ++i ) {
    visited[i] = false;
  }
  for ( int i{0}; i < order; ++i ) {
    for ( int j{0}; j < order; ++j ) {
      std::cin >> adjecency[i][j];
    }
  }

  for ( int victim{0}; victim < order; ++victim ) {
    if ( not visited[victim] ) {
      DFS_REC( victim );
    }
  }

  system("pause");
  return 0;
}
