#include<iostream>
#include<map>
#include<string>
#include<vector>
#include<algorithm>
#include<tuple>
using namespace std;
typedef tuple<int,int,int,int> ip;
typedef map<ip,map<ip,int>> mmap;

int count(map<ip,int> umap){
	int c=0;
	for(auto e:umap){
		c+=e.second;
	}
	return c;
}

void print_ip(ip _ip){
	std::cout<<get<0>(_ip)<<'.'<<get<1>(_ip)<<'.'<<get<2>(_ip)<<'.'<<get<3>(_ip);
}

bool cmp(pair<ip,int> a,pair<ip,int> b){
	if (a.second==b.second){
		if(get<0>(a.first)>get<0>(b.first))
			return false;
		if(get<1>(a.first)>get<1>(b.first))
			return false;
		if(get<2>(a.first)>get<2>(b.first))
			return false;
		return get<3>(a.first)<get<3>(b.first);
	}
	return a.second>b.second;
}

int main()
{
	size_t n;
	cin>>n;
	mmap m;
	int max_c=0;
	ip max_uip;
	map<ip,int> max_u;
	while(n--){
		ip uip, sip;
		{ // gathering uip and sip
			int a,b,c,d;
			cin>>a>>b>>c>>d;
			uip=make_tuple(a,b,c,d);
			cin>>a>>b>>c>>d;
			sip=make_tuple(a,b,c,d);
		}
		m[uip][sip]++;
		auto u=m[uip];
		auto c=count(u);
		if(c>max_c){
			max_u=u;
			max_uip=uip;
			max_c=c;
		}
	}
	vector<pair<ip,int>> v;
	v.reserve(max_u.size());
	for(auto e:max_u) v.push_back(e);
	std::sort(v.begin(),v.end(),cmp);
	print_ip(max_uip);
	std::cout<<'\n';
	for(auto e:v){
		std::cout<<e.second<<" - ";
		print_ip(e.first);
		std::cout<<'\n';
	}
}