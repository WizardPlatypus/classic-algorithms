#include<iostream>
using namespace std;
struct Option {
  int32_t moves=0;
  uint8_t last_c=0;
  uint64_t last=0;
};
int main(){
  int32_t n;
  uint64_t max_w;
  cin>>n>>max_w;
  uint64_t*w=new uint64_t[n];
  for(int32_t i=0;i<n;i++){
    cin>>w[i];
    if(w[i]>max_w){
      cout<<"Impossible";
      return 0;
    }
  }
  Option* best=new Option[1<<n];
  best[0]={0,0,0};
  for(uint64_t S=1ULL;S<(1ULL<<n);S++){
    best[S]={n+1,0,0};
    for(int p=0;p<n;p++){
      // if element p is in S
      if(S&(1ULL<<p))
      {
        Option option=best[S^(1ULL<<p)];
        // S^(1<<p) === set S without element p
        if (option.last_c<2 && option.last+w[p]<=max_w){
          option.last_c++;
          option.last+=w[p];
        } else {
          option.moves++;
          option.last_c=1;
          option.last=w[p];
        }
        if(best[S].moves>option.moves){
          best[S]=option;
        }
      }
    }
  }
  cout<<best[(1ULL<<n)-1].moves+1;
}