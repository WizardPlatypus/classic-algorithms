#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

int main()
{
  int n, k;
  cin >> n >> k;

  vector<int> a(n);
  for (auto &v : a)
  {
    cin >> v;
  }

  sort(a.begin(), a.end(), std::greater<int>());

  if (a.front() > k)
  {
    cout << "Impossible";
    return 0;
  }

  int c = 0;
  while (a.size() > 1){
    int l = 1;
    int r = a.size() - 1;
    int j = (r - l) / 2 + l;

    while (l < r)
    {
		if (a[0]+a[j] > k){
			l=j+1;
		} else if (a[0]+a[j]<k){
			r=j-1;
		} else break;
		j = (r - l) / 2 + l;
    }
	std:cerr<<"j: "<<j<<" "<<a[j]<<'\n';
    if (a[0]+a[j]<=k){
		std::cerr << a[0] << " " << a[j] << std::endl;
		a.erase(a.begin() + j);
	} else {
		std::cerr << a[0] << std::endl;
	}
    a.erase(a.begin());
    c++;
  }

  if (!a.empty())
  {
    std::cerr << a[0] << std::endl;
    c++;
  }

  std::cerr << std::endl;
  std::cout << c << std::endl;
  return 0;
}