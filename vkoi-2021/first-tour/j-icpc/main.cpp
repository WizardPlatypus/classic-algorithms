#include <iostream>
#include <cstring>
using namespace std;

const int MAX_N= 100;

int index(char c) {
	return (int)c - 65;
}

int main() {
	int fin[MAX_N], fail[MAX_N];
	for (auto& e: fin) {
		e= -1;
	}
	for (auto& e: fail) {
		e= 0;
	}
	int n;
	cin >> n;
	for (int _= 0; _ < n; _++) {
		int time;
		char problem;
		string status;
		cin >> time >> problem >> status;
		bool solved= status[0] == 'r';
		if (solved) {
			fin[index(problem)]= time;
		} else {
			fail[index(problem)]++;
		}
	}
	int c= 0, p= 0;
	for(int i= 0; i < n; i++) {
		if (fin[i] == -1) continue;
		c++;
		p += fin[i] + fail[i] * 20;
	}
	cout << c << ' ' << p;
}