#include <iostream>
using namespace std;
int main(){
  int t;
  cin>>t;
  while(t--){
    int n;
    cin>>n;
    bool*l=new bool[n];
    for(int i=0;i<n;i++){
      l[i]=0;
    }
    char f;
    cin>>f;
    if(f=='*'){
      l[0]=l[1]=1;
    }
    for(int i=1;i+1<n;i++){
      cin>>f;
      if(f=='*'){
        l[i-1]=l[i]=l[i+1]=1;
      }
    }
    cin>>f;
    if(f=='*'){
      l[n-2]=l[n-1]=1;
    }
    int c=0;
    for(int i=0;i+2<n;i++){
      if(!l[i]){
        c++;
        l[i]=l[i+1]=l[i+2]=1;
      }
    }
    if(!l[n-2]||!l[n-1]){
      c++;
    }
    cout<<c<<'\n';
  }
}