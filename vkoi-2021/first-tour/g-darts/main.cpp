#include <iostream>
using namespace std;
int main() {
  int t, c1{0}, c2{0}, s1{0}, s2{0};
  cin >> t;
  bool first= true;
  for (; t > 0; t--, first= !first) {
    int a, b, c;
    cin >> a >> b >> c;
    int s= a + b + c;
    if (s == 0)
      continue;
    if (s % 5 != 0) {
      continue;
    }
    s /= 5;
    if (first) {
      if (s1 + s > 51) {
        continue;
      }
      if (s1 + s == 51) {
        c1++;
        break;
      }
      s1 += s;
      c1++;
    } else {
      if (s2 + s > 51) {
        continue;
      }
      if (s2 + s == 51) {
        s2 += s;
        c2++;
        break;
      }
      s2 += s;
      c2++;
    }
  }
  if (first) {
    cout << 1 << ' ' << c1;
  } else {
    cout << 2 << ' ' << c2;
  }
}