#include <iostream>
#include <cstdlib>
using namespace std;
int gcd(int a, int b){
	while(b){
		auto t=b;
		b=a%b;
		a=t;
	}
	return a;
}
int brute_force(int n, int m) {
	int c=0;
	for (int i=0;i<m;i++){
		for(int j=0;j<n;j++){
			if (i*n+j==j*m+i) {
				printf("%d %d\n", i, j);
				c++;
			}
		}
	}
	return c;
}
int f(int n, int m) {
	if(n==1) return m;
	if(m==1) return n;
	int di,dj,g;
	g=gcd(n-1,m-1);
	//cerr<<"g: "<<g<<'\n';
	di=(n-1)/g;
	dj=(m-1)/g;
	//cerr<<"di dj: "<<di<<' '<<dj<<'\n';
	int c=0;
	for(int a=0,b=0;a<n&&b<m;a+=di,b+=dj){
		//printf("a b: %d %d\n", a, b);
		c++;
	}
	return c;
}
int main(){
	int n, m;
	cin >> n >> m;
	//cout<<"brute force: "<<brute_force(n, m)<<'\n';
	//cout<<"f: "<<f(n, m)<<'\n';
	cout<<f(n,m);
}