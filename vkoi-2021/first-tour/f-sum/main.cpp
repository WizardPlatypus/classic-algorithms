#include <iostream>
using namespace std;

typedef long long ll;
typedef unsigned long long ull;

#define max(a, b) (a > b ? a : b)
#define min(a, b) (a < b ? a : b)

const ll LINF = 1e18;

ull sum(ull a) {
	ull sum = 0;
	while (a > 0) {
	ull cur = a % 10;
	sum += cur;
	a /= 10;
	}
	return sum;
}

ull sum(ull n, ull i, ull l){
	switch (l)
	{
	case 1:{
		l+=1;
	} break;
	case 2:{
		i%=10;
		if(i==9) l-=16;
		else l+=2;
	} break;
	case 3:{
		i%=5;
		if(i==4) l-=6;
		else l+=3;
	} break;
	case 4:{
		i%=3;
		if(i==2) l-=5;
		else l+=4;
	} break;
	case 5:{
	} break;
	case 6:{
		i%=2;
		if(i) l-=3;
		else l+=6;
	} break;
	case 7:{
		i%=2;
		if(i) l-=2;
		else l+=7;
	} break;
	case 8:{
		i%=2;
		if(i) l-=1;
		else l+=8;
	} break;
	case 9:{
		i%=12;
		if(i==11) l-=9;
		else if(i==10) l+=9;
		else; // do nothing
	} break;
	}
	return l;
}

int main() {
	ull a, b;
	cin >> a >> b;
	for (ull n=a; n<=b;n++){
		cout<<n<<':';
		for(int i=1;31-i;i++){
			cout<<sum(n*i)<<' ';
		}
		cout<<'\n';
		/*
		ull res=0;
		ull the_end=LINF/n;
		ull l=sum(n);
		for (ull i = 2; i <= the_end; i++) {
			ull s1, s2;
			s1=sum(i*n);
			s2=sum(n, i, l);
			cout<<s1<<' '<<s2<<'\n';
			l=s2;
			if (sum(i*n) == i) {
				res=i*n;
				break;
			}
		}
		*/
		//cout << n << ' ' << res << ' ' << res / n << '\n'; // */
	}
	return 0;
}
