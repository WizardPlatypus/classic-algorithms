#include <iostream>
using namespace std;
// calculates how much time would it take to make n toys by a
// person (a,b,c)
int calc_time(int n, int a, int b, int c){
  return (n-1)/b*c+a*n;
}
// finds max number of toys that can be created by a person (a,b,c)
// in given time t
int find_n(int t, int a, int b, int c, int max_n){
  int n=0;
  for(int s=max_n/2;s>0;s/=2){
    while(calc_time(n+s,a,b,c)<=t){
      n+=s;
    }
  }
  return n;
}
// answers whether it's possible to complete n toys in time t
bool can_do(int t, int n, int m, int* a, int* b, int* c){
  if(t<=0) return false;
  int sum=0;
  for(int i=0;i<m;i++){
    sum+=find_n(t,a[i],b[i],c[i],n);
  }
  return sum>=n;
}
int main(){
  int n, m;
  cin>>n>>m;
  int* a=new int[m];
  int* b=new int[m];
  int* c=new int[m];
  // worst indicates a person with slowest performance
  int worst=0;
  for(int i=0;i<m;i++){
    cin>>a[i];
    cin>>b[i];
    cin>>c[i];
    if(a[i]>a[worst]){
      worst=i;
    } else if (a[i]==a[worst]){
      if(b[i]<b[worst]){
        worst=i;
      } else if(b[i]==b[worst]){
        if(c[i]>c[worst]){
          worst=i;
        }
      }
    }
  }
  // binary search, finds max time t for which friends
  // can't create n toys
  // /*
  int t=0;
  for(int s=calc_time(n,a[worst],b[worst],c[worst])/2;s>0;s/=2){
    while(!can_do(t+s,n,m,a,b,c)) t+=s;
  }
  cout<<t+1; // */
}