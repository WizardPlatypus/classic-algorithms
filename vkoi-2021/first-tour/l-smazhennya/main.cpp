#include <iostream>
using namespace std;
uint64_t f(uint64_t n, uint64_t k) {
	if (n == 0) return 0;
	if (n <= k) return 4;
	return (2*n + k -1) / k * 2;
}
int main() {
	uint64_t n, k;
	cin >> n >> k;
	/*
	for (uint64_t i= 1; i <= 50; i++) {
		cout<<i<<' '<<f(n, i)<<'\n';
	}
	// */
	cout << f(n, k);
	return 0;
}