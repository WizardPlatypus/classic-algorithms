#include <iostream>
#include <cstdlib>
using namespace std;

double T, X;
const double EPSILON = 1e-6;

double Mapping(double a_value, double a_begin, double a_end,
	double b_begin, double b_end) {
	double scale = (b_end - b_begin) / (a_end - a_begin);
	double offset = -a_begin * scale + b_begin;
	double b_value = a_value * scale + offset;
	return b_value;
}

double Efficency(double t) {
	if (t * 3 >= T) {
		return 100;
	}
	if (t * 6 >= T) {
		return Mapping(t, T / 6, T / 3, X, 100);
	}
	return Mapping(t, 0, T / 6, 0, X);
}

double Work(double t) {
	return Efficency(t) * (T - t);
}

double Search(double left, double right) {
	while (right - left > EPSILON) {
		double m1 = left + (right - left) / 3;
		double m2 = right - (right - left) / 3;
		if (Work(m1) < Work(m2))
			left = m1;
		else
			right = m2;
	}
	return (left + right) / 2;
}

int main() {
	cin >> X >> T;
	double result = Work(Search(0, T));
	printf("%.6f\n", result);
	return 0;
}