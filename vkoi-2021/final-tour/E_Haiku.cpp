#include <iostream>
#include <string>

using namespace std;

using ll = int;

bool isvowel(char c) {
	return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
}

int count_syllables(string word) {
	int count= 0;
	if (isvowel(word[0])) count++;
	for (size_t i= 1; i < word.size(); i++) {
		if (!isvowel(word[i - 1]) && isvowel(word[i])) {
			count++;
		}
	}
	return count;
}

int main() {
	int n;
	cin >> n;
	int* syllables= new int[n];
	for (int i= 0; i < n; i++) {
		string word;
		cin >> word;
		syllables[i]= count_syllables(word);
	}
	int count= 0;
	for (int i= 0; i < n; i++) {
		int word_i= i, k= 0;
		while (k < 5 && word_i < n) {
			k += syllables[word_i++];
		}
		if (k != 5) continue;
		k= 0;
		while (k < 7 && word_i < n) {
			k += syllables[word_i++];
		}
		if (k != 7) continue;
		k= 0;
		while (k < 5 && word_i < n) {
			k += syllables[word_i++];
		}
		if (k != 5) continue;
		count++;
	}

	cout << count << endl;
}
