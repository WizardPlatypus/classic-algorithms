#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

struct Point { int x{}, y{}; };
using Polygon = std::vector<Point>;
struct Segment {
    Point begin{}, end{};
    /* Answers whether this segment crosses vertical line which
     * goes through x. If does, then it stores y coord into y
     */
    bool crossing( const int x, int& y ) const {
        int min_x = std::min( begin.x, end.x );
        int max_x = std::max( begin.x, end.x );
        bool isInside = x >= min_x && x < max_x;
        if ( !isInside )
            return false;
        double k = (double)( end.y - begin.y )/(double)( end.x - begin.x );
        double b = begin.y - begin.x * k;
        y = (int)floor(x * k + b);
        return true;
    }
};
void CountCrossings(const Point, const std::vector<Segment>,
  int&, int&);

void CountCrossings(const Point target,
  const std::vector<Segment> segments, int& upper, int& lower) {
    for ( const Segment segment: segments ) {
        int y{};
        if ( segment.crossing( target.x, y ) ) {
            if ( y < target.y )
                lower++;
            else if ( y > target.y )
                upper++;
        }
    }
}

int main() {
    // eat points of polygon
    size_t length{};
    std::cin >> length;
    Polygon points{length};
    for ( Point& point: points ) {
        std::cin >> point.x >> point.y;
    }
    // define segments
    std::vector<Segment> segments{length};
    for ( size_t i = 0; i < length; ++i ) {
        size_t j = (i + 1) % length;
        segments[i] = Segment{points[i], points[j]};
    }
    // eat target point
    Point target{};
    std::cin >> target.x >> target.y;
    // count upper and lower intersections of line
    // going through target point with segments
    int upper{}, lower{};
    CountCrossings(target, segments, upper, lower);
    // answer the question
    //std::cout << upper << " " << lower << "\n";
    if ( upper % 2 == 1 && lower % 2 == 1 ) {
        std::cout << "Yes, it is inside polygon";
    } else {
        std::cout << "No, it's not inside polygon";
    }
    std::cout << "\n";
}
