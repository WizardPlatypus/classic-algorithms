#include <iostream> // cin, cout
#include <cmath> // pow
#include <algorithm> // max, min

int levelOf(int number) {
    int level = 1;
    while ( pow( 2, level) - 1 < number ) {
        level += 1;
    }
    return level;
}

void order( int& first, int& second ) {
    int min = std::min( first, second );
    int max = std::max( first, second );
    first = min;
    second = max;
}

int main()
{
    /* Tree looks like this:
     *             [1]
     *      (2              3)
     *  (4      5)     (6       7)
     *(8  9) (10 11) (12 13) (14 15)
     *... and so on
     * the goal is to find the distance between two elements of such tree.
     * as an example, distance between vertex 4 and 5 is 2, 10 and 3 is 4
     */

    ///# input
    /* begin - from wich vertex path begins
     * end - to wich vertex path goes
     * distance - sought-for distance between begin and end vertexes
     */
    int begin{}, end{}, distance{0};
    std::cin >> begin >> end;
    order( begin , end );

    // level_begin - level of begin vertex
    // level_end - level of end vertex
    int level_begin = levelOf( begin );
    int level_end = levelOf( end );

    // getting both begin and end vertexes on the same level
    distance += level_end - level_begin;
    level_end = level_begin;
    for ( int i = 0; i < distance; ++i ) end /= 2;

    // pulling both vertexes up, at some point they'll meet
    while ( begin != end ) {
        begin /= 2;
        end /= 2;
        distance += 2;
    }

    ///# output
    std::cout << "Distance = " << distance << std::endl;
    system("pause");
    return 0;
}
